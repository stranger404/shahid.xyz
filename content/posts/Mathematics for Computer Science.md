---
title: "Mathematics for Computer Science"
date: 2022-04-24T06:38:24+05:30
katex: true
markup: 'mmark'
draft: true
---
> __What is a Proof?__\
A <u>proof</u> is a method for ascertaining truth.

